﻿using System;
using System.Configuration;
using System.Reflection;

namespace ExchSportSettlement.modal
{
    public static class Utils
    {
        public static string GetServiceName
        {
            get
            {
                string serviceName = string.Empty;

                try
                {
                    Assembly executingAssembly = Assembly.GetAssembly(typeof(ProjectInstaller));
                    string targetDir = executingAssembly.Location;
                    Configuration config = ConfigurationManager.OpenExeConfiguration(targetDir);
                    serviceName = config.AppSettings.Settings["serviceName"].Value.ToString();

                    return serviceName;
                }
                catch (Exception ex)
                {
                    return "ExchSportSettlementService";
                }
            }
        }

        public static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["DataEntities"].ToString();
            }
        }
    }
}
