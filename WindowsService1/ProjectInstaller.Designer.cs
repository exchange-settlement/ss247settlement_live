﻿using ExchSportSettlement.modal;

namespace ExchSportSettlement
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ExchSportSettlementServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ExchSportSettlementServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ExchSportSettlementServiceProcessInstaller
            // 
            this.ExchSportSettlementServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ExchSportSettlementServiceProcessInstaller.Password = null;
            this.ExchSportSettlementServiceProcessInstaller.Username = null;
            // 
            // ExchSportSettlementServiceInstaller
            // 
            this.ExchSportSettlementServiceInstaller.Description = "Settling bets of market for " + Utils.GetServiceName;
            this.ExchSportSettlementServiceInstaller.DisplayName = Utils.GetServiceName;
            this.ExchSportSettlementServiceInstaller.ServiceName = Utils.GetServiceName;
            this.ExchSportSettlementServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ExchSportSettlementServiceProcessInstaller,
            this.ExchSportSettlementServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ExchSportSettlementServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller ExchSportSettlementServiceInstaller;
    }
}