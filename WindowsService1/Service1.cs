﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Timers;
using ExchSportSettlement.modal;

namespace ExchSportSettlement
{
    public partial class ExchSportSettlementService : ServiceBase
    {
        //Timer timer = new Timer();
        int timerTimeInMillisecond = Convert.ToInt32(ConfigurationManager.AppSettings["timerTimeInMillisecond"]);

        public ExchSportSettlementService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            //timer.Interval = timerTimeInMillisecond;
            //timer.Enabled = true;
            Task.Run(async () =>
            {
                while (true)
                {
                    try
                    {
                        try
                        {
                            EventLog.WriteEntry("1: " + "Start - " + DateTime.Now);
                        }
                        catch
                        {

                        }
                        await SettleMarket();
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            EventLog.WriteEntry("2: " + "error" + ex.ToString());
                        }
                        catch
                        {

                        }
                    }
                    finally
                    {
                        await Task.Delay(timerTimeInMillisecond);
                    }
                }
            });
        }

        protected override void OnStop()
        {
            // WriteToFile("Service is stopped at " + DateTime.Now);
        }

        //private void OnElapsedTime(object source, ElapsedEventArgs e)
        //{
        //    this.LogMessage();
        //}

        private async Task SettleMarket()
        {
            SqlConnection connection = null;
            SqlCommand command = null;
            try
            {
                //string strConnection = Utils.ConnectionString;
                //connection = new SqlConnection(@"Server=86.106.157.186,1533;Initial Catalog=Satsport247_Live;Persist Security Info=True;User ID=user_satsport247_live;Password=Satsportlive@321;MultipleActiveResultSets=False;Connection Timeout=30;");
                connection = new SqlConnection(Utils.ConnectionString);
                command = new SqlCommand("dbo.GetSettleableMarket", connection);
                command.CommandType = CommandType.StoredProcedure;

                await connection.OpenAsync();

                SqlDataAdapter adp = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string marketType = dt.Rows[i]["MarketType"].ToString().ToLower(); // Change Field Name 
                    string bfSportId = dt.Rows[i]["BfSportID"].ToString(); // Change Field Name 
                    if (bfSportId == "7")
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleHorseRacing", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            await cmd.ExecuteNonQueryAsync();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Exception In SettleHorseRacing", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }

                    }
                    else if (marketType == "line")
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleLineMarket", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            await cmd.ExecuteNonQueryAsync();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Exception In SettleLineMarket", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }

                    }
                    else if (marketType == "advancesession" || marketType == "session")
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleSession", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            await cmd.ExecuteNonQueryAsync();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Exception In SettleSession", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }

                    }
                    else if (marketType == "bookmakers")
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleBookMakers", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            await cmd.ExecuteNonQueryAsync();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Exception In SettleBookMakers", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }

                    }
                    else if (marketType == "premium_odds")
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettlePremiumOdds", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            await cmd.ExecuteNonQueryAsync();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Exception In SettlePremiumOdds", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }
                    }
                    else if (marketType == "manualodds" || marketType == "manual_odds")
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleManualOdds", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            await cmd.ExecuteNonQueryAsync();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Exception In SettlePremiumOdds", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }
                    }
                    else
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("SettleMatchOdds", connection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@MarketId", SqlDbType.VarChar).Value = dt.Rows[i]["MarketId"].ToString();
                            cmd.Parameters.Add("@Result", SqlDbType.VarChar).Value = dt.Rows[i]["Result"].ToString();
                            await cmd.ExecuteNonQueryAsync();
                        }
                        catch (Exception ex)
                        {
                            EventLog.WriteEntry("Exception In SettleMatchOdds", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Exception In Settlement", "Message : " + ex.Message.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                command.Dispose();
                connection.Dispose();
            }
        }
    }
}
