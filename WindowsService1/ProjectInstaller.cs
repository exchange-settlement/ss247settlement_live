﻿using ExchSportSettlement.modal;
using System.ComponentModel;
using System.ServiceProcess;

namespace ExchSportSettlement
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
            ExchSportSettlementServiceInstaller.AfterInstall += (sender, args) => new ServiceController(Utils.GetServiceName).Start();
        }
    }
}
